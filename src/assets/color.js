export const ClassumRed = '#ef484a';
export const ClassumBlack = '#2d3e51';
export const ClassumYellow = '#f5a623';
export const ClassumBlue = '#3c96ff';
export const ClassumGray = '#9b9b9b';
export const HintofRed = '#fcf9fa';
